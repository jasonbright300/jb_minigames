﻿using System;

namespace Assets.Scripts.Dota3.View
{
    public interface IGameLocker 
    {
        bool IsLocked { get; }
    }

    [Serializable]
    public class IGameLockerContainer : IUnifiedContainer<IGameLocker> { }
}
