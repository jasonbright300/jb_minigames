﻿using Assets.Scripts.Dota3.Controller;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Dota3.View
{
    public class HeroStatBar : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _attackPowerLabel;
        [SerializeField] private Image _healthBar;

        public Hero Current { get; private set; }

        public void Set( Hero hero )
        {
            Current = hero;

            _attackPowerLabel.text = hero.AttackPower.ToString();
            UpdateHealthBar();
        }

        private void UpdateHealthBar()
        {
            _healthBar.fillAmount = (float) Current.Health / Current.MaxHealth;
        }

        void Update()
        {
            if (Current == null)
                return;
            UpdateHealthBar();
        }
    }
}
