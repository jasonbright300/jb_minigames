﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DOTween = DG.Tweening.DOTween;

namespace Assets.Scripts.Dota3.View
{
    public class ResultBox : MonoBehaviour, IGameLocker
    {
        public bool IsLocked => _hitSequence != null && _hitSequence.IsPlaying();

        [SerializeField] private Image _sword;
        [SerializeField] private TextMeshProUGUI _damageLabel;
        [SerializeField] private CanvasGroup _canvasGroup;
    
        [SerializeField] private Color _playerColor;
        [SerializeField] private Color _enemyColor;

        [SerializeField] private float _moveAnimTime;
        [SerializeField] private float _fadeAnimTime;

        private Sequence _hitSequence;

        private Vector2? _defaultPosition;

        void Start()
        {
            GameRoot.Instance.Game.Enemy.Hit.AddListener( OnEnemyHit );
            GameRoot.Instance.Game.Player.Hit.AddListener( OnPlayerHit );
        }

        private void OnPlayerHit(int dmg)
        {
            PlayHitAnimation( dmg, false );
        }

        private void OnEnemyHit(int dmg)
        {
            PlayHitAnimation( dmg, true );          
        }

        private void PlayHitAnimation(int dmg, bool isPlayerAttack)
        {
            if (_hitSequence != null && _hitSequence.IsPlaying())
                _hitSequence.Kill(true);

            _hitSequence = DOTween.Sequence();

            _hitSequence.Append( Set( dmg, isPlayerAttack ) ).AppendInterval( 0.5F ).Append( Hide() );
        }

        private Sequence Set( int damage, bool isPlayerAttack )
        {
            var isMiss = damage <= 0;
        
            _damageLabel.text = isMiss ? "MISS" : damage.ToString();

            var color = isPlayerAttack ? _playerColor : _enemyColor;

            _sword.color = color;
            _damageLabel.color = color;

            _sword.gameObject.SetActive( !isMiss );

            if (!isMiss)
            {
                _sword.transform.localScale = new Vector3( isPlayerAttack ? -1 : 1, 1, 1);
                if (isPlayerAttack)
                {
                    _sword.transform.SetAsFirstSibling();
                }
                else
                {
                    _sword.transform.SetAsLastSibling();
                }
            }

            //--------- anim ---------------
            var sequence = DOTween.Sequence();
            var rt = transform as RectTransform;

            if (_defaultPosition.HasValue == false)
                _defaultPosition = rt.anchoredPosition;

            rt.anchoredPosition = _defaultPosition.Value - Vector2.down * 300;

            sequence.Append( rt.DOAnchorPos( _defaultPosition.Value, _moveAnimTime ) )
                .Insert( 0, _canvasGroup.DOFade( 1, _fadeAnimTime ) );

            return sequence;
        }

        public Tween Hide()
        {
            return _canvasGroup.DOFade( 0, _fadeAnimTime );
        }
    }
}
