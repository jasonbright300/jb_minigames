﻿
using System.Runtime.Remoting.Messaging;
using Assets.Scripts.Dota3.Controller;
using Assets.Scripts.Dota3.Model;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Game = Assets.Scripts.Dota3.Controller.Game;

namespace Assets.Scripts.Dota3.View
{
    public class GameRoot : MonoSingleton<GameRoot>
    {
        [SerializeField] private Image _attackBar;
        [SerializeField] private Image _defBar;

        [SerializeField] private float _gameSpeed;

        [SerializeField] private HeroStatBar _enemyStats;
        [SerializeField] private HeroStatBar _playerStats;

        [SerializeField] private IGameLockerContainer _resultBox;

        public Game Game { get; private set; }

        public HeroProp Enemy;
        public HeroProp Player;

        private bool _isAllowUpdateGame => _resultBox.Result == null || !_resultBox.Result.IsLocked;

        protected override void Init()
        {
            base.Init();

            Game = new Game( Enemy, Player, _gameSpeed );

            _enemyStats.Set( Game.Enemy );
            _playerStats.Set( Game.Player );
        }

        void Update()
        {
            if (_isAllowUpdateGame)
            {
                UpdateGame();
            }

            if (Game == null)
                return;

            _attackBar.fillAmount = Game.PlayerAttack;
            _defBar.fillAmount = Game.EnemyDeffence;
        }

        void UpdateGame()
        {
            if (Game == null)
                return;

            Game.Tick( Time.deltaTime );

            if (Input.GetMouseButtonDown( 0 ))
            {
                Game.AttackEnemy();
            }
        }
    }
}