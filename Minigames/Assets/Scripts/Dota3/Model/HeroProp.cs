﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Dota3.Model
{
    [CreateAssetMenu(menuName = "Dota 3 - Create Hero Prop")]
    public class HeroProp : ScriptableObject
    {
        public Sprite Icon;
        public int AttackPower;
        public int MaxHealth;
        public int Armor;
    }
}
