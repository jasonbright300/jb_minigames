﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Dota3.Controller
{
    public interface IAttacker
    {
        float Power { get; }
    }
}
