﻿using UnityEngine;

namespace Assets.Scripts.Dota3.Controller
{
    public class PlayerAttacker : IAttacker, ITickable
    {
        private const float MAX_ATTACK_RATIO = 2;
        private const float BASE_POWER = 0.3F;

        public float Power { get; private set; }

        private float _targetPower;
        private readonly float _additionalAttackRatio;

        private readonly float _speed;

        public PlayerAttacker( Hero hero, Hero enemy, float speed )
        {
            _additionalAttackRatio = ((float) hero.AttackPower / enemy.AttackPower) - 1.0F;
            _speed = speed;

            _targetPower = GetNewTarget();
        }

        public void Tick( float deltaTime )
        {
            Power = Mathf.Lerp( Power, _targetPower, deltaTime * _speed );
            if (Mathf.Abs( Power - _targetPower ) < 0.01F)
            {
                _targetPower = GetNewTarget();
            }
        }

        private float GetNewTarget()
        {
            var ratioPercent = Mathf.Clamp01( _additionalAttackRatio / MAX_ATTACK_RATIO );
            if (_additionalAttackRatio > 0)
            {
                var basePower = ratioPercent * BASE_POWER;
                return Mathf.Clamp01( Random.Range( basePower, 1.0F ) );
            }
            else
            {
                return Mathf.Clamp01( Random.Range( 0.0F, ratioPercent ) );
            }
        }
    }
}