﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Dota3.Controller
{
    public class TimeManager
    {
        private readonly List<ITickable> _tickables = new List<ITickable>();

        public void Add( ITickable entity )
        {
            if (entity == null)
                return;

            if (_tickables.Contains( entity ))
                return;

            _tickables.Add( entity );
        }

        public void Remove( ITickable entity )
        {
            if (!_tickables.Contains( entity ))
                return;

            _tickables.Remove( entity );
        }

        public void Tick( float deltaTime )
        {
            foreach (var tickable in _tickables)
            {
                tickable.Tick( deltaTime );
            }
        }
    }
}
