﻿using Assets.Scripts.Dota3.Model;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Dota3.Controller
{
    public class Game
    {
        public Hero Enemy { get; private set; }
        public Hero Player { get; private set; }

        public float PlayerAttack => _playerBehaviour.Power;
        public float EnemyDeffence => _enemyBehaviour.Power;

        public float GameSpeed { get; private set; }

        public EndGameEventHandler GameEnded = new EndGameEventHandler();

        private readonly TimeManager _timeManager = new TimeManager();

        private bool _isEnemyTurn = false;

        private IAttacker _playerBehaviour;
        private IAttacker _enemyBehaviour;

        public Game( HeroProp enemy, HeroProp player, float gameSpeed )
        {
            Enemy = new Hero( enemy.MaxHealth, enemy.AttackPower, enemy.Armor )
            {
                Prop = enemy
            };

            Player = new Hero( player.MaxHealth, player.AttackPower, player.Armor )
            {
                Prop = player
            };

            GameSpeed = gameSpeed;
            StartBattle();
        }

        public void StartBattle()
        {
            _playerBehaviour = new PlayerAttacker( Player, Enemy, GameSpeed );
            _timeManager.Add( (ITickable) _playerBehaviour );

            _enemyBehaviour = new EnemyAttacker( Enemy, GameSpeed );
            _timeManager.Add( (ITickable)_enemyBehaviour );
        }

        public void AttackEnemy()
        {
            var power = Mathf.Clamp( _playerBehaviour.Power - _enemyBehaviour.Power, 0, int.MaxValue);
                
            var dmg = Mathf.CeilToInt( Player.AttackPower *  power);
            Enemy.TakeDamage( dmg );
            if (!Enemy.IsAlive)
            {
                FinishBattle();
            }
            _isEnemyTurn = true;
        }

        public void AttackPlayer()
        {
            const float missChance = 0.15F;
            var dmg = Mathf.CeilToInt( Random.Range( 0, Enemy.AttackPower ) - Enemy.AttackPower * missChance );
            Player.TakeDamage( dmg );
            if (!Player.IsAlive)
            {
                FinishBattle();
            }
        }

        public void FinishBattle()
        {
            var args = new EndGameEventArgs();
            GameEnded.Invoke( args );
        }

        public void Tick( float deltaTime )
        {
            _timeManager.Tick( deltaTime );
            if (_isEnemyTurn)
            {
                AttackPlayer();
                _isEnemyTurn = false;
            }
        }
    }

    public class EndGameEventHandler : UnityEvent<EndGameEventArgs> { }
    public class EndGameEventArgs 
    {

    }
}