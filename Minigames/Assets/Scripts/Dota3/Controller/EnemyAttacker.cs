﻿using UnityEngine;

namespace Assets.Scripts.Dota3.Controller
{
    public class EnemyAttacker : IAttacker, ITickable
    {
        private const float MAX_ARMOR = 20.0F;

        public float Power { get; private set; }

        private float _speed;
        private float _baseArmor;

        private float _targetPower;

        public EnemyAttacker( Hero entity, float speed )
        {
            _speed = speed;
            _baseArmor = entity.Armor / MAX_ARMOR;
        }

        public void Tick( float deltaTime )
        {
            Power = Mathf.Lerp( Power, _targetPower, deltaTime * _speed );
            if (Mathf.Abs( Power - _targetPower ) < 0.01)
            {
                _targetPower = GetNewTarget();
            }
        }

        private float GetNewTarget()
        {
            return Mathf.Clamp01( Random.Range( _baseArmor, 1.0F ) );
        }
    }
}
