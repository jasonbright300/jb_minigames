﻿using Assets.Scripts.Dota3.Model;
using UnityEngine.Events;

namespace Assets.Scripts.Dota3.Controller
{
    public class Hero
    {
        public HeroProp Prop;

        public int MaxHealth { get; private set; }
        public int Health { get; private set; }

        public int AttackPower { get; private set; }
        public int Armor { get; private set; }

        public bool IsAlive => Health > 0;

        public IntUnityEvent Hit = new IntUnityEvent();
        public UnityEvent Died = new UnityEvent();

        public Hero( int maxHealth, int attackPower, int armor )
        {
            MaxHealth = maxHealth;
            Health = MaxHealth;
            AttackPower = attackPower;
            Armor = armor;
        }

        public void TakeDamage( int damage )
        {
            Health -= damage;
            Hit.Invoke( damage );
            if (Health <= 0)
            {
                Health = 0;
                Died.Invoke();
            }
        }
    }
}
