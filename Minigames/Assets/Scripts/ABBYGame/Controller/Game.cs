﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Random = System.Random;

namespace Assets.Scripts.ABBYGame.Controller
{
    public class Game
    {
        public const string LETTERS = "qwertyuiopasdfghjklzxcvbnm";

        public int Difficult { get; private set; }
        public List<char> CurrentLetters { get; private set; }
        public char[] CurrentPool { get; private set; }

        public float Timer { get; private set; }
        public float AdditionalTimePerLetter { get; private set; }

        public bool IsAlive => Timer > 0;

        public LetterEvent CorrectInputed = new LetterEvent();
        public LetterEvent WrongInputed = new LetterEvent();

        public UnityEvent TimerElapsed = new UnityEvent();

        private string _randomSortedLetters;


        public Game( int startDifficult, float startTimer, float additionalTimePerLetter )
        {
            _randomSortedLetters = RandomSort( LETTERS );

            SetDifficult( startDifficult );

            CurrentLetters = new List<char>();
            GenerateLetters();

            Timer = startTimer;
            AdditionalTimePerLetter = additionalTimePerLetter;
        }

        public bool Input( char symbol )
        {
            if (!IsAlive)
                return false;

            if (CurrentLetters.Count == 0)
                return false;

            bool equal = symbol.ToString().Equals(CurrentLetters[0].ToString(), 
                StringComparison.InvariantCultureIgnoreCase);
            if (equal)
            {
                var ch = CurrentLetters[0];
                CurrentLetters.RemoveAt( 0 );
                CorrectInputed.Invoke( ch );

                Timer += AdditionalTimePerLetter;
                return true;
            }

            WrongInputed.Invoke( symbol );
            return false;
        }

        public void Tick( float deltaTime )
        {
            if (IsAlive)
            {
                Timer -= Time.deltaTime;
                if (Timer <= 0)
                {
                    TimerElapsed.Invoke();
                    Timer = 0;
                }
            }
        }

        public void SetDifficult( int difficult )
        {
            Difficult = Mathf.Clamp( difficult, 1, LETTERS.Length );
            CurrentPool = _randomSortedLetters.Substring( 0, Difficult ).ToCharArray();
        }

        public void GenerateLetters( int count = 10 )
        {
            for (int i = 0; i < count; i++)
            {
                var rnd = UnityEngine.Random.Range( 0, CurrentPool.Length - 1 );
                CurrentLetters.Add( CurrentPool[rnd] );
            }
        }

        private string RandomSort( string str )
        {
            var arr = str.ToCharArray();
            Random rnd = new Random();
            return new string( arr.OrderBy( x => rnd.Next() ).ToArray() );
        }
    }

    public class LetterEvent : UnityEvent<char> { }
}
