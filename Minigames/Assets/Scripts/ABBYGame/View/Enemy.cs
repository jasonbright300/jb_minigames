﻿using UnityEngine;

namespace Assets.Scripts.ABBYGame.View
{
    public class Enemy : MonoBehaviour
    {
        public Transform Hero;
        public float AdditionalDistance;

        void Start()
        {
            GameRoot.Instance.EventMessenger.GameEnded.AddListener( OnGameEnded );
        }

        private void OnGameEnded()
        {
            var animators = GetComponentsInChildren<Animator>();
            foreach (var animator in animators)
            {
                animator.SetTrigger( "Attack" );
            }
        }

        void Update()
        {
            if (GameRoot.Instance.Game == null || !GameRoot.Instance.Game.IsAlive)
                return;
            
            var distance = Mathf.Abs( Hero.transform.position.x - transform.position.x ) + AdditionalDistance;
            var time = GameRoot.Instance.Game.Timer;
            var speed = distance / time;
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
    }
}
