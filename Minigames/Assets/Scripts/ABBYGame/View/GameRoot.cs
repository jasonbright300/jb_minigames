﻿using System;
using System.Collections.Generic;
using Assets.Scripts.ABBYGame.Controller;
using Assets.Scripts.ABBYGame.View.UI;
using Pooling;
using UnityEngine;

namespace Assets.Scripts.ABBYGame.View
{
    public class GameRoot : MonoSingleton<GameRoot>
    {
        public GameSettings GameSettings;
        
        public Game Game { get; private set; }
        public readonly EventMessenger EventMessenger = new EventMessenger();

        [Header( "UI" )] 
        [SerializeField] private DifficultWindow DifficultWindow;

        [Space( 10 )] 
        [SerializeField] private Transform _lettersContainer;
        [SerializeField] private Letter _letterPrefab;
        [SerializeField] private float _letterSpacing;
        [SerializeField] private ParticleSystem _letterVfx;

        private ObjectPool<Letter> _lettersPool;

        private List<Letter> _currentLetters = new List<Letter>();
 
        private Queue<char> _correctInputQueue = new Queue<char>();

        private bool _isHeroIdle = true;
 
        public Vector3 GetFirstLetterPosition()
        {
            return _currentLetters[0].transform.position;
        }

        protected override void Init()
        {
            base.Init();

            ShowDifficultWindow();
        }

        private void ShowDifficultWindow()
        {
            DifficultWindow.gameObject.SetActive( true );
            DifficultWindow.DifficultSelected.AddListener( OnDifficultSelected );
        }

        private void CreateGame( int difficult )
        {
            Game = new Game( difficult, GameSettings.StartTimer, GameSettings.AdditionalTimePerLetter );
            Game.CorrectInputed.AddListener( OnCorrectInput );
            Game.TimerElapsed.AddListener( () => EventMessenger.GameEnded.Invoke() );

            foreach (var letter in Game.CurrentLetters)
            {
                CreateLetter( letter );
            }

            EventMessenger.LetterAttacked.AddListener( OnLetterAttacked );
            EventMessenger.MoveEnded.AddListener( OnMoveEnded );
        }

        private void OnMoveEnded()
        {
            _correctInputQueue.Dequeue();
            _isHeroIdle = true;
            TryToAttackNext();
        }

        private void OnLetterAttacked()
        {
            var letter = _currentLetters[0];
            letter.DestroyAnimation( () => _lettersPool.Push( letter ));
            _letterVfx.transform.position = letter.transform.position;
            _letterVfx.Play(true);

            _currentLetters.RemoveAt( 0 );
        }

        private void CreateLetter(char letter)
        {
            if(_lettersPool == null)
                _lettersPool = new ObjectPool<Letter>( _letterPrefab, 0, _lettersContainer );

            var letterObj = _lettersPool.Pop();
            letterObj.Set( letter );

            letterObj.transform.localPosition = new Vector3( _letterSpacing * _currentLetters.Count, 0 );

            _currentLetters.Add( letterObj );
        }

        private void OnCorrectInput( char letter )
        {
            _correctInputQueue.Enqueue( letter );
            TryToAttackNext();
        }

        private bool TryToAttackNext()
        {
            if (_correctInputQueue.Count == 0)
                return false;

            if (_isHeroIdle)
            {
                EventMessenger.AttackBegun.Invoke();
                _isHeroIdle = false;
            }
            return true;
        }

        private void OnDifficultSelected( int difficult )
        {
            CreateGame( difficult );

            DifficultWindow.gameObject.SetActive( false );
            DifficultWindow.DifficultSelected.RemoveListener( OnDifficultSelected );
        }

        void Update()
        {
            if (Game != null)
            {
                if (Input.anyKeyDown && Input.inputString.Length > 0)
                {
                    Game.Input( Input.inputString[0] );
                }
                Game.Tick( Time.deltaTime );
            }
        }
    }
}
