﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace Assets.Scripts.ABBYGame.View
{
    public class CameraController : MonoBehaviour
    {
        public Transform Hero;
        public float TargetX = 0.0F;
        public float LimitX = 4.0F;
        public float CameraFixTime;

        private Tween _fixCameraTween;

        void Start()
        {
            FixCameraPosition( 0 );
        }

        void Update()
        {
            var inverse = transform.InverseTransformPoint( Hero.position );
            if (inverse.x > LimitX && _fixCameraTween == null)
            {
               FixCameraPosition( CameraFixTime );
            }
        }

        public void FixCameraPosition( float time )
        {
            var inverse = transform.InverseTransformPoint( Hero.position );
            _fixCameraTween = transform.DOMoveX( transform.position.x + inverse.x + TargetX, time )
                .OnComplete( () => _fixCameraTween = null );
        }
    }
}
