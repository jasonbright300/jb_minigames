﻿using UnityEngine;

namespace Assets.Scripts.ABBYGame.View
{
    public class ParallaxController : MonoBehaviour
    {
        public Transform Hero;
        public ScrollingScript Scrolling;

        public float ParallaxSpeed;

        private float? _heroPrevPositionX;


        void Update()
        {
            var curPos = Hero.transform.position.x;

            if (_heroPrevPositionX.HasValue)
            {
                var deltaPosition = curPos - _heroPrevPositionX.Value;
                Scrolling.speed = Vector2.right * deltaPosition * ParallaxSpeed;
            }

            _heroPrevPositionX = curPos;
        }
    }
}
