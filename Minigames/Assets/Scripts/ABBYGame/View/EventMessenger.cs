﻿using UnityEngine.Events;

namespace Assets.Scripts.ABBYGame.View
{
    public class EventMessenger
    {
        public UnityEvent AttackBegun = new UnityEvent();
        public UnityEvent GameEnded = new UnityEvent();
        public UnityEvent MoveEnded = new UnityEvent();
        public UnityEvent LetterAttacked = new UnityEvent();
    }
}
