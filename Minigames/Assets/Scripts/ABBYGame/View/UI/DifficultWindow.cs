﻿using UnityEngine;

namespace Assets.Scripts.ABBYGame.View.UI
{
    public class DifficultWindow : MonoBehaviour
    {
        public IntUnityEvent DifficultSelected;

        public void OnDifficultButton( int level )
        {
            var difficultLevels = GameRoot.Instance.GameSettings.DifficultLevels;
            level = Mathf.Clamp( level, 0, difficultLevels.Length - 1 );
            var difficult = difficultLevels[level];
            DifficultSelected.Invoke( difficult );
        }
    }
}