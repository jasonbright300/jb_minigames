﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.ABBYGame.View
{
    public class Hero : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private float _movingTime;
        [SerializeField] private float _distanceToLetter;

        void OnEnable()
        {
            GameRoot.Instance.EventMessenger.AttackBegun.AddListener( Attack );
            GameRoot.Instance.EventMessenger.GameEnded.AddListener( Die );
        }

        void OnDisable()
        {
            try
            {
                GameRoot.Instance.EventMessenger.AttackBegun.RemoveListener( Attack );
                GameRoot.Instance.EventMessenger.GameEnded.RemoveListener( Die );
            }
            catch{ /*nothing */ }
        }

        void OnStart()
        {
            transform.position = GetPositionNearLetter();
        }

        public void Attack()
        {
            _animator.SetTrigger( "Attack" );
            StartCoroutine( DoMove() );
        }

        public void Die()
        {
            StartCoroutine( DoDie() );
        }

        public void OnAttackFinished()
        {
            GameRoot.Instance.EventMessenger.LetterAttacked.Invoke();
        }

        private Vector3 GetPositionNearLetter()
        {
            return new Vector3( GameRoot.Instance.GetFirstLetterPosition().x - _distanceToLetter, transform.position.y,
                transform.position.z );
        }

        IEnumerator DoDie()
        {
            yield return new WaitForSeconds( 0.25F );
            _animator.SetTrigger( "Die" );
        }

        IEnumerator DoMove()
        {
            yield return new WaitForSeconds( 0.1F );
            while (_animator.GetCurrentAnimatorStateInfo( 0 ).IsName( "_ATTACK" ))
            {
                yield return null;
            }

            var startPos = transform.position;
            var endPos = GetPositionNearLetter();
            var t = 0.0F;
            while (t < _movingTime)
            {
                t += Time.deltaTime;
                var progress = t / _movingTime;
                transform.position = Vector3.Lerp( startPos, endPos, progress );
                yield return null;
            }
            _animator.SetTrigger( "WalkEnd" );
            GameRoot.Instance.EventMessenger.MoveEnded.Invoke();
        }
    }
}