﻿using UnityEngine;

namespace Assets.Scripts.ABBYGame.View
{
    [CreateAssetMenu( menuName = "ABBYGame - Create Game Settings")]
    public class GameSettings : ScriptableObject
    {
        public int[] DifficultLevels => _difficultLevels;
        public float StartTimer => _startTimer;
        public float AdditionalTimePerLetter => _additionalTimePerLeter;

        [SerializeField] private int[] _difficultLevels;
        [SerializeField] private float _startTimer;
        [SerializeField] private float _additionalTimePerLeter;
    }
}
