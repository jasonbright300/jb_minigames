﻿using System;
using DG.Tweening;
using Pooling;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.ABBYGame.View
{
    public class Letter : MonoBehaviour, IResetable
    {
        [SerializeField] private TextMeshPro _text;
        [SerializeField] private float _destroyAnimationTime;

        public char Current { get; private set; }

        public void Set( char letter )
        {
            _text.text = letter.ToString();
            Current = letter;
        }

        public void DestroyAnimation(Action onComplete)
        {
            transform.DOScale( Vector3.zero, _destroyAnimationTime ).OnComplete( onComplete.Invoke );
        }

        public void Reset()
        {
            transform.localScale = Vector3.one;
        }
    }
}
