﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Bubbles.ViewController
{
    public class GameRoot : MonoBehaviour
    {
        [SerializeField] private Transform[] _borders;

        [SerializeField] private Ball _ballPrefab;
        [SerializeField] private Catcher _catcherPrefab;

        [Space( 20 )] 
        [SerializeField] private TextMeshProUGUI _resultLabel;

        private List<Ball> _balls;
        private List<Catcher> _catchers = new List<Catcher>();

        private bool _isGameStarted = false;

        private int _score;

        void Start()
        {
            _balls = new List<Ball>();

            for (int i = 0; i < 5; i++)
            {
                var ball = Instantiate( _ballPrefab );
                ball.Direction = new Vector2( Random.Range( -10, 10 ), Random.Range( -10, 10 ) ).normalized;
                ball.Speed = Random.Range( 7.0F, 13.0F );
                _balls.Add( ball );
            }
        }

        void Update()
        {
            var removeBalls = new List<Ball>();
            foreach (var ball in _balls)
            {
                var nextPosition = GetNextPosition( ball.Position, ball.Direction, ball.Speed );

                for (int i = 0; i < _borders.Length; i++)
                {
                    var cur = _borders[i];
                    var next = i + 1 >= _borders.Length ? _borders[0] : _borders[i + 1];

                    if (Math2d.LineSegmentsIntersection( cur.position, next.position, ball.Position, nextPosition,
                        out var intersect ))
                    {
                        var borderVector = next.position - cur.position;
                        var borderNormal = new Vector2( borderVector.y, -borderVector.x ).normalized;
                        var reflectDirection = Vector2.Reflect( ball.Direction, borderNormal ).normalized;
                        nextPosition = GetNextPosition( ball.Position, reflectDirection, ball.Speed );
                        ball.Direction = reflectDirection;
                        break;
                    }
                }

                foreach (var catcher in _catchers)
                {
                    if (GetCircleIntersection( ball.Position, nextPosition, catcher.Position,
                        catcher.Radius, out var intersection ))
                    {
                        SpawnCatcher(intersection, ball.Color);
                        removeBalls.Add(ball);

                        if (_score == 0)
                            _score = 100;
                        _score = _score * 2;
                        break;
                    }
                }

                ball.transform.position = nextPosition;
            }

            foreach (var ball in removeBalls)
            {
                RemoveBall( ball );
            }

            if (Input.GetMouseButtonDown( 0 ))
            {
                if (_isGameStarted == false)
                {
                    SpawnCatcher( Camera.main.ScreenToWorldPoint(Input.mousePosition), Color.white );
                    _isGameStarted = true;
                }
            }

            if (IsGameFinished())
            {
                enabled = false;
                _resultLabel.text = "Your score: " + _score;
            }
        }

        private bool IsGameFinished()
        {
            if (_isGameStarted && (_balls.Count == 0 || _catchers.Count == 0))
                return true;
            return false;
        }

        private void SpawnCatcher( Vector2 position, Color color )
        {
            var catcher = Instantiate( _catcherPrefab, position, Quaternion.identity );
            _catchers.Add( catcher );
            catcher.TimeElapsed.AddListener( OnCatcherElapsed  );
            catcher.SetColor( color );
        }

        private void RemoveCatcher( Catcher catcher )
        {
            _catchers.Remove( catcher );
            catcher.TimeElapsed.RemoveListener( OnCatcherElapsed );
            Destroy( catcher.gameObject );
        }

        private void OnCatcherElapsed( Catcher target )
        {
            RemoveCatcher( target );
        }

        private void RemoveBall( Ball ball )
        {
            _balls.Remove( ball );
            Destroy( ball.gameObject );
        }

        private Vector2 GetNextPosition( Vector2 currentPos, Vector2 direction, float speed )
        {
            return currentPos + direction * speed * Time.deltaTime;
        }

        private bool GetCircleIntersection( Vector2 p1, Vector2 p2, Vector2 center, float radius, out Vector2 intersection )
        {
            var dots = new[] {p1, p2};
            foreach (var dot in dots)
            {
                var dist = Vector2.Distance( dot, center );
                if (dist > radius)
                    continue;

                intersection = dot + (dot - center) * (dist - radius);
                return true;
            }

            intersection = Vector2.zero;
            return false;
        }
    }
}