﻿using UnityEngine;

namespace Assets.Scripts.Bubbles.ViewController
{
    public class Ball : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;

        public float Speed;
        public Vector2 Direction;

        public Vector2 Position => transform.position;
        public Color Color => _renderer.color;

        void Start()
        {
            _renderer.color = new Color( Random.Range( 0, 1.0F ), Random.Range( 0, 1.0F ), Random.Range( 0, 1.0F ), 1.0F );
        }
    }
}
