﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Bubbles.ViewController
{
    public class Catcher : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _renderer;
        [SerializeField] private float _maxTimer;
        [SerializeField] private AnimationCurve _sizeCurve;

        public Vector2 Position => transform.position;
        public float Radius => _renderer.bounds.extents.x;
        public CatcherEventHandler TimeElapsed = new CatcherEventHandler();

        private float _timer;

        void Awake()
        {
            transform.localScale = Vector3.one *_sizeCurve.Evaluate( 0 );
        }

        void Update()
        {
            _timer += Time.deltaTime;
            if (_timer >= _maxTimer)
            {
                TimeElapsed.Invoke( this );
                enabled = false;
            }
            transform.localScale = Vector3.one * _sizeCurve.Evaluate( _timer / _maxTimer );
        }

        public void SetColor( Color color )
        {
            _renderer.color = color;
        }
    }

    public class CatcherEventHandler : UnityEvent<Catcher> {}
}
