﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.EqualLines.Model;
using UnityEngine.Events;

namespace Assets.Scripts.EqualLines.ViewController
{
    public class EventMessenger
    {
        public readonly LineItemEventHandler ItemPut = new LineItemEventHandler();
    }

    public class LineItemEventHandler : UnityEvent<LineItemEventArgs> {}

    public class LineItemEventArgs
    {
        public ItemData Item;
        public ItemElement Source;
        public LineElement Target;
        public Line Line;
    }
}
