﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.EqualLines.Model;
using Assets.Scripts.EqualLines.Model.Buffs;

namespace Assets.Scripts.EqualLines.ViewController.Buffs
{
    public class DoubleItemBuff : IBuff
    {
        private DoubleItemsBuffData _data;
        private LineElement _target;
        private Line _targetLine;

        public int Score { get; private set; }

        public void Apply( BuffData data, LineElement element, Line Line )
        {
            _data = data as DoubleItemsBuffData;
            _target = element;
            _targetLine = Line;

            GameRoot.Instance.EventMessenger.ItemPut.AddListener( OnItemPut );
        }

        private void OnItemPut( LineItemEventArgs args )
        {
            if (args.Line != _targetLine)
                return;

            RefreshBuffs();
        }

        private void RefreshBuffs()
        {
            var doubleItems = _targetLine.Elements.Where( x => x.CurrentItem == _data.ItemType );

            var count = doubleItems.Count();

            if (count < 2)
                return;

            var rate = count  * (_data.Rate - 1) - 1;

            Score = _data.ItemType.BasePower * rate;

            _target.SetItem( _target.CurrentItem );
        }        
    }
}