﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.EqualLines.Model;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

namespace Assets.Scripts.EqualLines.ViewController
{
    public class GameRoot : MonoSingleton<GameRoot>
    {
        [SerializeField] private int _lineSize;
        [SerializeField] private Line[] _lines;
        [SerializeField] private LineElement _lineElementPrefab;

        [Space( 10 )] 
        [SerializeField] private int _itemsCount;
        [SerializeField] private List<ItemData> _itemsPool;
        [SerializeField] private ItemElement _itemPrefab;
        [SerializeField] private RectTransform _itemsContainer;

        [Space( 10 )]
        [SerializeField] private TextMeshProUGUI _resultLabel;

        private List<ItemElement> _items;

        public EventMessenger EventMessenger { get; private set; }

        protected override void Init()
        {
            base.Init();
            EventMessenger = new EventMessenger();

            foreach (var line in _lines)
            {
                InitLine( line, _lineSize );
            }

            var pool = GetPool( _itemsCount, _itemsPool );
            _items = new List<ItemElement>();

            foreach (var itemData in pool)
            {
                var item = Instantiate(_itemPrefab, _itemsContainer);
                item.Set(itemData);
                _items.Add(item);
            }
        }

        private List<ItemData> GetPool( int count, List<ItemData> allItems )
        {
            var min = allItems.Min( x => x.BasePower );
            var max = allItems.Max( x => x.BasePower );

            var maxMultiplier =  Mathf.FloorToInt(  ((float)(max * count) / min) * 0.9F ); //1 - вычисляем максимальную сумму всех элементов
                                                                                    //2 - делим сумму на минимальный элемент и получаем максимальное значение рандома
                                                                                    //3 - умножаем его на число < 1 чтобы не возникало ситуации, когда все
                                                                                    //элементы будут c макс.значением
            var multiplier = Random.Range( 10, maxMultiplier);
            while( multiplier % 2 != 0)
            {
                multiplier = Random.Range( 10, 26 );
            }

            var intPoolLeft = new List<int>();
            for(int i  = 0; i < multiplier / 2; i++)
                intPoolLeft.Add( min );

            var intPoolRight = new List<int>(intPoolLeft);

            bool isLeft = true;
            
            var freeCount = (intPoolLeft.Count + intPoolRight.Count) - count;
            while (freeCount > 0)
            {
                var pool = isLeft ? intPoolLeft : intPoolRight;

                var rand = Random.Range( 2, freeCount > pool.Count ? pool.Count : freeCount );

                var rnd = new System.Random();
                pool = pool.OrderBy(x =>
                {
                    //сортируем так, чтобы максимальные значения были в конце массива
                    var rNum = rnd.Next( max );
                    return x == max ? max : rNum;
                } ).ToList();

                var newSum = 0;
                var tryNewSum = 0;
                var sumCount = 0;
                for (int i = 0; i < rand; i++)
                {
                    tryNewSum = tryNewSum + pool[i];
                    if (allItems.Any( x => x.BasePower == tryNewSum ))
                    {
                        newSum = tryNewSum;
                        sumCount++;
                    }
                }

                for (int i = 0; i < sumCount; i++)
                {
                    pool.RemoveAt( 0 );
                }
                pool.Add( newSum );

                if (isLeft)
                {
                    intPoolLeft = pool;
                }
                else
                {
                    intPoolRight = pool;
                }

                isLeft = !isLeft;
                freeCount = (intPoolLeft.Count + intPoolRight.Count) - count;
            }

            var resultPool = new List<ItemData>();

            for (int i = 0; i < 2; i++)
            {
                var pool = i == 0 ? intPoolLeft : intPoolRight;
                foreach (var intItem in pool)
                {
                    resultPool.Add( allItems.First(x => x.BasePower == intItem) );
                }
            }

            return resultPool;
        }

        private void OnItemPutToLine( LineItemEventArgs args )
        {
            _items.Remove( args.Source );
            Destroy( args.Source.gameObject );
            EventMessenger.ItemPut.Invoke( args );
            UpdateResultLabel();
        }

        private void InitLine( Line line, int size )
        {
            line.Elements = new List<LineElement>(size);
            for (int i = 0; i < size; i++)
            {
                var element = Instantiate( _lineElementPrefab, line.Container );
                line.Elements.Add( element );
                element.CurrentLine = line;
                element.ItemPut.AddListener( OnItemPutToLine );
            }
        }

        private void UpdateResultLabel()
        {
            var str = new StringBuilder();
            for (int i = 1; i < _lines.Length; i++)
            {
                var prevLineScore = _lines[i - 1].GetScore();
                var currentLineScore = _lines[i].GetScore();
                str.AppendLine( prevLineScore.ToString() );
                str.AppendLine( prevLineScore == currentLineScore ? "=" : "!=" );
                str.AppendLine( currentLineScore.ToString() );
            }
            _resultLabel.text = str.ToString();
        }
    }
}