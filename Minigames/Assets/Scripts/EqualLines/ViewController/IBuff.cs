﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.EqualLines.Model;

namespace Assets.Scripts.EqualLines.ViewController
{
    public interface IBuff
    {
        void Apply( BuffData data, LineElement element, Line Line );
        int Score { get; }
    }
}