﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Assets.Scripts.EqualLines.Model;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.EqualLines.ViewController
{
    public class LineElement : MonoBehaviour, IDropHandler
    {
        [SerializeField] private Image _icon;
        [SerializeField] private TextMeshProUGUI _powerLabel;

        public Line CurrentLine;
        public LineItemEventHandler ItemPut = new LineItemEventHandler();

        public ItemData CurrentItem { get; private set; }

        private List<IBuff> _buffs = new List<IBuff>();

        public void AddBuff( IBuff buff )
        {
            if (!_buffs.Contains( buff ))
            {
                _buffs.Add( buff );
                SetItem( CurrentItem );
            }
        }

        public void RemoveBuff( IBuff buff )
        {
            _buffs.Remove( buff );
        }

        public void OnDrop( PointerEventData eventData )
        {
            var itemElement = eventData.pointerDrag.GetComponent<ItemElement>();
            if (itemElement != null)
            {
                SetItem( itemElement.Current );
                var buff = itemElement.Current.Buff?.GetBuff();
                if (buff != null)
                {
                    AddBuff( buff );
                    buff.Apply( itemElement.Current.Buff, this, CurrentLine  );
                }

                var eventArgs = new LineItemEventArgs()
                {
                    Item = itemElement.Current,
                    Source = itemElement,
                    Target = this,
                    Line = CurrentLine,
                };
                ItemPut.Invoke( eventArgs );
            }
        }

        public void SetItem( ItemData itemData )
        {
            CurrentItem = itemData;
            _icon.sprite = itemData.Icon;
            _icon.color = Color.white;
            var fullPower = GetFullPower();
            _powerLabel.text = fullPower.ToString();
        }

        public int GetFullPower()
        {
            var power = CurrentItem.BasePower;
            foreach (var buff in _buffs)
            {
                power += buff.Score;
            }

            return power;
        }
    }
}