﻿using Assets.Scripts.EqualLines.Model;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.EqualLines.ViewController
{
    public class ItemElement : MonoBehaviour, IDragHandler, IEndDragHandler
    {
        [SerializeField] private Image _icon;
        [SerializeField] private TextMeshProUGUI _powerLabel;
        [SerializeField] private RectTransform _target;

        public ItemData Current { get; private set; }

        public void Set( ItemData data )
        {
            Current = data;
            _icon.sprite = data.Icon;
            _powerLabel.text = data.BasePower.ToString();
        }

        public void OnDrag( PointerEventData eventData )
        {
            _target.anchoredPosition += eventData.delta;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        public void OnEndDrag( PointerEventData eventData )
        {
            _target.anchoredPosition = Vector2.zero;
            GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
    }
}
