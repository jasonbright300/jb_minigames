﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.EqualLines.ViewController
{
    [System.Serializable]
    public class Line
    {
        public RectTransform Container;
        public List<LineElement> Elements;

        public int GetScore()
        {
            var score = 0;
            foreach (var element in Elements)
            {
                if (element.CurrentItem != null)
                    score += element.GetFullPower();
            }

            return score;
        }
    }
}