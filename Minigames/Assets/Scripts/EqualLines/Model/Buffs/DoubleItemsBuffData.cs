﻿using Assets.Scripts.EqualLines.ViewController;
using Assets.Scripts.EqualLines.ViewController.Buffs;
using UnityEngine;

namespace Assets.Scripts.EqualLines.Model.Buffs
{
    [CreateAssetMenu(menuName = "EqualLines - Create DoubleItemsBuff Asset")]
    public class DoubleItemsBuffData : BuffData
    {
        [SerializeField] private ItemData _itemType;
        [SerializeField] private int _rate;

        public ItemData ItemType => _itemType;
        public int Rate => _rate;

        public override IBuff GetBuff()
        {
            return new DoubleItemBuff();
        }
    }
}