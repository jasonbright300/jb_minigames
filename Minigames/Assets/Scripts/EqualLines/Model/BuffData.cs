﻿using Assets.Scripts.EqualLines.ViewController;
using UnityEngine;

namespace Assets.Scripts.EqualLines.Model
{
    public class BuffData : ScriptableObject
    {
        [SerializeField] private string _description;

        public string Description => _description;

        public virtual IBuff GetBuff()
        {
            return null;
        }
    }
}
