﻿using UnityEngine;

namespace Assets.Scripts.EqualLines.Model
{
    [CreateAssetMenu(menuName = "EqualLines - Create ItemData")]
    public class ItemData : ScriptableObject
    {
        [SerializeField] private Sprite _icon;
        [SerializeField] private int _power;
        [SerializeField] private BuffData _buff;

        public Sprite Icon => _icon;
        public int BasePower => _power;
        public BuffData Buff => _buff;
    }
}
