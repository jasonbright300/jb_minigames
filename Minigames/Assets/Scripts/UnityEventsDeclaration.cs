﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class IntUnityEvent : UnityEvent<int> {}

[System.Serializable]
public class BoolUnityEvent : UnityEvent<bool> {}
