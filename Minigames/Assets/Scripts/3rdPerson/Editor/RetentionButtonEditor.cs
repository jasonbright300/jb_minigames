﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.UI;

[CustomEditor(typeof(RetentionButton))]
public class RetentionButtonEditor : ButtonEditor
{

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        EditorGUILayout.PropertyField( serializedObject.FindProperty("OnRetentionStart") );
        EditorGUILayout.PropertyField(serializedObject.FindProperty("OnRetentionEnd"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("OnRetention"));
        serializedObject.ApplyModifiedProperties();

    }
}
