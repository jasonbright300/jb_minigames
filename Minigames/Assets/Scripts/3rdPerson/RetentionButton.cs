﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class RetentionButton : Button
{
    public UnityEvent OnRetentionStart;
    public UnityEvent OnRetentionEnd;
    public UnityEvent OnRetention;

    enum States
    {
        Down,
        Retention,
        Stop
    }

    private States _state = States.Stop;
    private bool _pointerInside;

    private int _currentPointerId;
    
    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        _pointerInside = true;
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);

        if (_currentPointerId != eventData.pointerId)
            return;

        if (_state == States.Retention)
        {
            if (interactable)
                OnRetentionEnd.Invoke();
            _state = States.Stop;
        }

        _pointerInside = false;
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        base.OnPointerDown(eventData);

        if (_state != States.Stop)
            return;

        Debug.Log("DOWN " + eventData.pointerId + "  " + eventData.position);
        _state = States.Down;
        if (_pointerInside)
        {
            if (interactable)
                OnRetentionStart.Invoke();
            _currentPointerId = eventData.pointerId;
            _state = States.Retention;
        }
        
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);

        if (_currentPointerId != eventData.pointerId)
            return;

        Debug.Log("POINTER UP " + _state  + " " + eventData.pointerId );
        if (_state == States.Retention)
        {
            if (interactable)
                OnRetentionEnd.Invoke();
            _state = States.Stop;
        }
           
    }

    void Update()
    {
        if (_state == States.Retention)
        {
            if(interactable)
                OnRetention.Invoke();
        }
            
    }
}
