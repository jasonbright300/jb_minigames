﻿using UnityEngine;

/// <summary>
/// Взято из https://pixelnest.io/tutorials/2d-game-unity/parallax-scrolling/
/// </summary>
public static class RendererExtensions
{
    public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
    }
}
